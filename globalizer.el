;;; globalizer.el -- Add your lovely globalize .t with style

;; Author: Ed Sinjiashvili <mediogre@gmail.com>
;; Keywords: rails i18n

;;; Commentary:
;; Scan through a buffer to find strings eligible for translation. 
;; Allow user to observe changes and make an educated choice: approve, veto,
;; quit or make changes automatically. Basically it does everything
;; query-replace does, but calls user-defined hook when replace
;; actually happens.
;; 
;; In other words if you choose the string to be translatable, it will
;; add .t to it (make it globalized) and also add the string to
;; another buffer which you can later give to a native translator to
;; create actual translations.

;;; Code:
(defconst globalize:buffer-extension ".globalines"
  "extension to append to buffer being globalized")

;;; TODO: 
;; - find a way to skip literals with .t (kinda hard without lookahead support)
;; - maybe match more complex ruby strings
;; - match symbols?
(defconst globalize:string-literal-regexp
  "\\(['\"]\\)\\(.*?\\)\\1"
  "a simple ruby literal string regexp")

(defconst globalize:replacement-string
  "\\&.t"
  "just add .t method call")

(defun globalize:get-buffer-name ()
  "get the name for a file used for storing globalized strings"
  (concat (buffer-file-name) globalize:buffer-extension))

(defun globalize:get-buffer ()
  "get buffer with globalized strings"
  (interactive)
  (let ((buffer (find-buffer-visiting (globalize:get-buffer-name))))
    (if (null buffer)
	(setq buffer (find-file-noselect (globalize:get-buffer-name)))
      buffer)))

(defun globalize:append-line (str)
  "append `str' to globalized buffer"
  (interactive)
  (save-current-buffer
    (set-buffer (globalize:get-buffer))
    (goto-char (point-max))
    (insert str)
    (insert "\n")))

(defun globalize:append-matched-string (&optional pos)
  "Append a matched (or some part of) string to globalize buffer"
  (or pos (setq pos 2))
  (globalize:append-line (match-string-no-properties pos)))

;; can't make it work - 
;; (defmacro globalize:save-case-fold (&rest body)
;;   "save current `case-fold-search' value and evaluate expression with
;; `case-fold-search' set to nil"
;;   `(let ((prev-case-fold case-fold-search)
;; 	 result)
;;      (setq case-fold-search nil)
;;      (setq result (progn ,body))
;;      (setq case-fold-search prev-case-fold)
;;      result))


;;;###autoload
(defun globalize:query-globalize ()
  "Globalize current buffer. Add .t to candidate lines and dump them
  to corresponding .globalines buffer for later use."
  (interactive)
  (ad-unadvise 'replace-match-maybe-edit)

  (defadvice replace-match-maybe-edit
    (before globalize:run-replaced-hook (newtext fixedcase literal noedit match-data))
    "Insert a matched (and approved) string to the globalize buffer"
    (run-hooks 'globalize:replaced-hook))

  (ad-activate 'replace-match-maybe-edit)

  ; clean all hooks on definition
  (set 'globalize:replaced-hook nil)

  (add-hook 'globalize:replaced-hook 'globalize:append-matched-string)

  (let ((prev-case case-fold-search))
    (setq case-fold-search nil)
    (query-replace-regexp
     globalize:string-literal-regexp
     globalize:replacement-string)
    (setq case-fold-search prev-case))

  (ad-unadvise 'replace-match-maybe-edit)

  (pop-to-buffer (globalize:get-buffer)))

(defun globalize:globalize-region (beg end)
  "Globalize selected region: erbify, .tify and add to globalines"
  (interactive "r")
  (globalize:append-line (buffer-substring-no-properties beg end))

  (let ((prev-case case-fold-search))
    (setq case-fold-search nil)
    (replace-regexp ".*" "<%= '\\&'.t %>" nil beg end)
    (setq case-fold-search prev-case))
  
  ;(pop-to-buffer (globalize:get-buffer))
)

(provide 'globalizer)

;;; globalizer.el ends here
